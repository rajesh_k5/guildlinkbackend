using GuildLinkTask.Controllers;

using GuildlinkTaskData.Interface;
using GuildlinkTaskData.Repository;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using System;

namespace APITestProject
{
    [TestClass]
    public class GuildlinkTaskTest
    {
        private IGuildlinkTaskInterface _guildlinktask = new GuildlinkTaskRepository();
        private DateTime fromDate = Convert.ToDateTime("2021-07-16");
        private DateTime toDate = Convert.ToDateTime("2021-07-19");
        private string jurisdiction = "nsw";
        private string apiUrl = "https://data.gov.au/data/api/3/action/datastore_search?resource_id=33673aca-0857-42e5-b8f0-9981b4755686";


        [TestMethod]
        public  void Task_GetWeekDays_Return_OkResult()
        {
            //Arrange 
           
            bool isIncludeDate = true;

            //Act
            int data = _guildlinktask.Weekdays(fromDate, toDate, jurisdiction, isIncludeDate,apiUrl);

            //Assert
            int okResult = data;
            Assert.IsNotNull(data);
            Assert.AreEqual(2,okResult);

        }

        [TestMethod]
        public void Task_GetWeekDays_ExcludeDates_Return_OkResult()
        {
            //Arrange 
          
            bool isIncludeDate = false;

            //Act
            int data = _guildlinktask.Weekdays(fromDate, toDate, jurisdiction, isIncludeDate, apiUrl);

            //Assert
            int okResult = data;
            Assert.IsNotNull(data);
            Assert.AreEqual(0, okResult);

        }
    }
}
