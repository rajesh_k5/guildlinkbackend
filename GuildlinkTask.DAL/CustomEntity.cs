﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuildlinkTask.DAL
{
    public class CustomEntity
    {
        public class CustomResponse
        {
            public bool Status { get; set; }
            public string Response { get; set; }
        }
    }
}
