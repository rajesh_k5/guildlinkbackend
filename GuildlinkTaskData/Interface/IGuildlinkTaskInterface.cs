﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuildlinkTaskData.Interface
{
    public interface IGuildlinkTaskInterface
    {
        public int Weekdays(DateTime startDate, DateTime endDate, string jurisdiction, bool isIncluded,string apiUrlforHolidayList);
    }
}
