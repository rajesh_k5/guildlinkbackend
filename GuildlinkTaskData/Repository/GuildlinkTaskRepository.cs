﻿using GuildlinkTaskData.Common;
using GuildlinkTaskData.Interface;
using GuildlinkTaskData.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace GuildlinkTaskData.Repository
{
    public class GuildlinkTaskRepository : IGuildlinkTaskInterface
    {


        HttpClientService _httpClient = HttpClientService.GetInstance;
       
        public int Weekdays(DateTime startDate, DateTime endDate, string jurisdiction, bool isIncluded, string holidayUrl)
        {
           
            int dayDiff = 0;
            try
            {
              
                var res = _httpClient.DownloadAndDeserializeJsonData<Root>(holidayUrl).result.records.ToList();
                var filterDatebyJurisdiction = res.Where(i => (i.Jurisdiction).Equals(jurisdiction.ToLower()) &&
                                           ConvertDate(i.Date) > startDate
                                            && ConvertDate(i.Date)
                                            < endDate).
                                            Select(j => ConvertDate(j.Date)).ToList();
                    

                dayDiff = (int)(endDate.Date - startDate.Date).TotalDays;
                if (isIncluded)
                {
                    dayDiff = dayDiff + 1; //  add 1 in day difference in case of include form and to date 
                }
                else
                {
                    filterDatebyJurisdiction.Add(startDate);
                    filterDatebyJurisdiction.Add(endDate);

                }


                return Enumerable.Range(0, dayDiff)
                                .Select(a => startDate.AddDays(a))
                                .Where(a => a.DayOfWeek != DayOfWeek.Sunday && a.DayOfWeek != DayOfWeek.Saturday)
                                 .Count(a => !filterDatebyJurisdiction.Any(x => x == a));

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private DateTime ConvertDate(string _date)
        {
            try
            {
                DateTime convertedDate = Convert.ToDateTime(DateTime.ParseExact(_date.ToString(),
                                               "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                return convertedDate;
            }
            catch(Exception ex)
            {
                throw ex;
            }
         
        }
    }

   
}









