﻿using GuildlinkTaskData.Model;

using Nancy.Json;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GuildlinkTaskData.Common
{
    public class HttpClientService
    {

        private static HttpClientService instance = new HttpClientService();
        public static HttpClientService GetInstance
        {
            get
            {
               
                return instance;
            }
        }

        private HttpClientService()
        {
            
        }
        public Root DownloadAndDeserializeJsonData<Root>(string url) where Root : new()
        {
            using (var webClient = new WebClient())
            {
                var jsonData = string.Empty;
                try
                {
                    jsonData = webClient.DownloadString(url);
                    return JsonConvert.DeserializeObject<Root>(jsonData);
                }
                catch (Exception ex)
                {
                    throw ex;
                }


               

            }
        }

        }
}
        


  
