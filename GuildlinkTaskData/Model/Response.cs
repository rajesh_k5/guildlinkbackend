﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuildlinkTaskData.Model
{
   public class Response
    {
        bool IsSuccess = false;
        string Message;
        object ResponseData;

        public Response(bool status, string message, object data)
        {
            IsSuccess = status;
            Message = message;
            ResponseData = data;


        }
    }
}
