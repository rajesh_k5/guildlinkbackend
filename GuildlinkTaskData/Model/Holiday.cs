﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Text;

namespace GuildlinkTaskData.Model
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Field
    {
        public string type { get; set; }
        public string id { get; set; }
    }

    public class Record
    {
        public int _id { get; set; }
        public string Date { get; set; }

        [JsonProperty("Holiday Name")]
        public string HolidayName { get; set; }
        public string Information { get; set; }

        [JsonProperty("More Information")]
        public string MoreInformation { get; set; }
        public string Jurisdiction { get; set; }
    }

    public class Links
    {
        public string start { get; set; }
        public string next { get; set; }
    }

    public class Result
    {
        public bool include_total { get; set; }
        public string resource_id { get; set; }
        public List<Field> fields { get; set; }
        public string records_format { get; set; }
        public List<Record> records { get; set; }
        public Links _links { get; set; }
        public int total { get; set; }
    }

    public class Root
    {
        public string help { get; set; }
        public bool success { get; set; }
        public Result result { get; set; }
    }


}
