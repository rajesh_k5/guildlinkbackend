﻿
using GuildlinkTaskData.Interface;
using GuildlinkTaskData.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System;

namespace GuildLinkTask.Controllers
{
    [Route("GuildlinkTaskAPI/")]

    public class GuildlinkTaskAPIController : Controller
    {
        private IGuildlinkTaskInterface _guildlinktask;
        private readonly ILogger<GuildlinkTaskAPIController> _logger;
        private readonly IConfiguration _configuration;



        public GuildlinkTaskAPIController(ILogger<GuildlinkTaskAPIController> logger, IConfiguration configuration, IGuildlinkTaskInterface guildlinktask)
        {
            _logger = logger;
            _configuration = configuration;
            _guildlinktask = new GuildlinkTaskRepository();
        }



        [HttpGet]
              
        public IActionResult GetWeekDays(DateTime startDate,DateTime endDate,string jurisdiction, bool isIncluded)
         {
           
            string apiUrlforHolidayList = _configuration["ApiUrl:url"];
            try
            {
                int result = _guildlinktask.Weekdays(startDate, endDate,jurisdiction,isIncluded, apiUrlforHolidayList);
                return Ok(Content(result.ToString(), "application/json"));
            }
            catch (Exception ex)
            {
                 _logger.LogInformation(ex.ToString());
                return StatusCode(StatusCodes.Status500InternalServerError,ex.Message);
            }
           
            
        }

        
    }
}
