﻿using GuildlinkTask.IAL;

using System;
using System.Collections.Generic;
using System.Text;

using static GuildlinkTask.DAL.CustomEntity;

namespace GuildLinkTask.BAL
{
   public class GuildlinkTaskApiBAL:IGuildlinkTaskAPI
    {
       
          public CustomResponse GetAllData()
         {
            CustomResponse response = new CustomResponse();
            try
            {
                response.Status = true;
                response.Response = "API called successfully";
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Response = "Exception occoured";
            }
            return response;
        }

    }
}
